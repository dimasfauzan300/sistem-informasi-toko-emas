<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">

				</div>
				<div class="col-sm-6">

				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-12">


				<div class="card">
					<div class="card-header">
						<h3 class="card-title"><b>Data Master Group</b></h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="col-lg-12 mb-3 d-flex justify-content-end">
								<!-- Button trigger modal -->
								<button type="button" class="btn btn-primary btn-md" data-toggle="modal"
									data-target="#modelId" id="tombol-tambah">
									<i class="fa fa-plus"></i> Tambah Data
								</button>
							</div>
						</div>
						<table id="mytable" class="table table-bordered table-striped" width="100%">
							<thead>
								<tr>
									<th class="text-center">ID Group</th>
									<th class="text-center">Nama</th>
									<th class="text-center">Aksi</th>
								</tr>
							</thead>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>

<?php $this->load->view('layouts/footer') ?>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
	<!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<form action="#" id="data-formulir">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Data Supplier</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-12">
							<div class="form-group">
								<label for="nama">Nama</label>
								<input type="text" name="nama" class="form-control" id="nama"
									aria-describedby="namaHelp" required>
								<small id="namaHelp" class="form-text text-red"></small>
							</div>
							
						</div>						
					</div>

					<input type="hidden" name="id_group" id="id_group" value="">
					<input type="hidden" name="opsiModal" id="opsiModal" value="">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
					<button class="btn btn-primary" id="simpan-data">Simpan</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script src="<?= base_url('assets/template') ?>/plugins/jquery/jquery.min.js"></script>
<script src="<?= base_url('assets/template') ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url('assets/template') ?>/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url('assets/template') ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="<?= base_url('assets/template') ?>/dist/js/adminlte.min.js"></script>
<script src="<?= base_url('assets/template') ?>/dist/js/demo.js"></script>
<script src="<?= base_url('assets/template') ?>/plugins/sweetalert2/sweetalert2.all.min.js"></script>
<script>
	$(document).ready(function () {

		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000
		});

		$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
			return {
				"iStart": oSettings._iDisplayStart,
				"iEnd": oSettings.fnDisplayEnd(),
				"iLength": oSettings._iDisplayLength,
				"iTotal": oSettings.fnRecordsTotal(),
				"iFilteredTotal": oSettings.fnRecordsDisplay(),
				"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
				"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
			};
		};

		$('#mytable').DataTable({
			processing: true,
			serverSide: true,
			ordering: true,
			ajax: {
				"url": "<?= base_url('group/dataTable') ?>",
				type: 'POST'
			},
			columns: [{
					"data": "id_group",
					className: 'text-center'
				},
				{
					"data": "nama_group",
					className: 'text-center'
				},
				{
					"data": "action",
					className: 'text-center'
				}
			]
		});


		$("#mytable_filter").addClass('d-flex justify-content-end');
		$("#mytable_filter>label>input").removeClass('form-control-sm');
		$("[name=mytable_length]").removeClass('form-control-sm');
		$("[name=mytable_length]").removeClass('custom-select-sm');

		function reset() {
			$("#id_group").val('');
			$("#nama").val('');
		}

		$("#tombol-tambah").on('click', function (e) {
			e.preventDefault();
			$(".modal-title").html("Tambah Data Group");
			$("#opsiModal").val('tambah');
			reset();
		});

		$(this).on('click', "#buton_edit", function (e) {
			e.preventDefault();
			let id_group = $(this).data('id');
			$.ajax({
				type: "get",
				url: `<?= base_url('group/edit/') ?>${id_group}`,
				dataType: "json",
				success: function (response) {
					$("#opsiModal").val('update');
					$("#nama").val(response.nama_group);
					$("#id_group").val(id_group);
				},
				error: function () {
					Toast.fire({
						type: 'error',
						title: 'Gagal mengambil data !'
					})
				}
			});
			$(".modal-title").html("Ubah Data Group");
		});

		$(this).on('click', '#buton_hapus', function (e) {
			e.preventDefault();
			let id_group = $(this).data('id');
			Swal.fire({
				title: 'Peringatan',
				text: "Apakah anda yakin akan menghapus data ?",
				type: 'warning',
				showCancelButton: true,
				buttonsStyling: false,
				confirmButtonClass: 'btn btn-danger btn-lg mr-2',
				cancelButtonClass: 'btn btn-default btn-lg',
				confirmButtonText: '<i class="fa fa-trash"></i> Hapus',
				cancelButtonText: 'Batal'
			}).then((result) => {
				if (result.value) {
					$.ajax({
						type: "DELETE",
						url: `<?= base_url('group/delete/') ?>${id_group}`,
						data: {
							_token: '{{csrf_token()}}'
						},
						dataType: "json",
						success: function (response) {
							let oTable = $('#mytable').dataTable();
							oTable.fnDraw(false);
						},
						error: function () {
							Toast.fire({
								type: 'error',
								title: 'Gagal menghapus data !'
							})
						}
					});
				}
			})
		});

		$("#simpan-data").on('click', function (e) {
			$('#simpan-data').html("Menyimpan...");
			$('#simpan-data').attr('disabled', true);
			let opsi = $("#opsiModal").val();
			let data = $("#data-formulir").serialize();
			if (opsi == "tambah") {
				tambah(data);
			} else {
				update(data);
			}
		});

		function tambah(data) {
			$.ajax({
				type: "POST",
				url: "<?= base_url('group/simpan') ?>",
				data: data,
				dataType: "json",
				success: function (response) {
					$('#simpan-data').html("Simpan");
					$('#simpan-data').removeAttr('disabled');
					let oTable = $('#mytable').dataTable();
					oTable.fnDraw(false);
					Toast.fire({
						type: 'success',
						title: 'Berhasil menyimpan data !'
					});
					$("#modelId").modal('hide');
				},
				error: function () {
					$('#simpan-data').html("Simpan");
					$('#simpan-data').removeAttr('disabled');
					Toast.fire({
						type: 'error',
						title: 'Gagal menyimpan data !'
					});
				}
			});
		}

		function update(data) {
			$.ajax({
				type: "PUT",
				url: "<?= base_url('group/update') ?>",
				data: data,
				dataType: "json",
				success: function (response) {
					$('#simpan-data').html("Simpan");
					$('#simpan-data').removeAttr('disabled');
					let oTable = $('#mytable').dataTable();
					oTable.fnDraw(false);
					Toast.fire({
						type: 'success',
						title: 'Berhasil mengupdate data !'
					});
					$("#modelId").modal('hide');
				},
				error: function () {
					$('#simpan-data').html("Simpan");
					$('#simpan-data').removeAttr('disabled');
					Toast.fire({
						type: 'error',
						title: 'Gagal menyimpan data !'
					});
				}
			});
		}
	});
</script>
</body>

</html>
