<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
	<!-- Brand Logo -->
	<a href="#" class="brand-link">
		<img src="<?= base_url('assets/template') ?>/dist/img/AdminLTELogo.png" alt="AdminLTE Logo"
			class="brand-image img-circle elevation-3" style="opacity: .8">
		<span class="brand-text font-weight-light">SI Toko Asih</span>
	</a>

	<!-- Sidebar -->
	<div class="sidebar">
		<!-- Sidebar user (optional) -->
		<div class="user-panel mt-3 pb-3 mb-3 d-flex">
			<div class="image">
				<img src="<?= base_url('assets/template') ?>/dist/img/avatar5.png" class="img-circle elevation-2"
					alt="User Image">
			</div>
			<div class="info">
				<a href="#" class="d-block"><?= $this->session->userdata('role')?></a>
			</div>
		</div>

		<!-- Sidebar Menu -->
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
				<!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
				<?php if($this->session->userdata('role') == 'Supervisor'){ ?>
				<li class="nav-item">
					<a href="<?= base_url('supervisor/beranda') ?>" class="nav-link <?= $active == 'beranda' ? 'active':'' ?>">
						<i class="nav-icon fas fa-home"></i>
						<p>
							Beranda
						</p>
					</a>
				</li>
				<li class="nav-item <?= $active == 'supplier' || $active == 'jenis_perhiasan' || $active == 'akun' || $active == 'group' ? ' menu-open has-treeview':'' ?>">
					<a href="#" class="nav-link">
						<i class="nav-icon fas fa-th-list"></i>
						<p>
							Data Master
							<i class="right fas fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?= base_url('supplier') ?>" class="nav-link <?= $active == 'supplier' ? 'active':'' ?>">
								<i class="far fa-address-book nav-icon"></i>
								<p>Data Suppliers</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?= base_url('jenis_perhiasan') ?>"
								class="nav-link  <?= $active == 'jenis_perhiasan' ? 'active':'' ?>">
								<i class="fa fa-tag nav-icon"></i>
								<p>Data Jenis Perhiasan</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?= base_url('group') ?>" class="nav-link <?= $active == 'group' ? 'active':'' ?>">
								<i class="fa fa-bookmark nav-icon"></i>
								<p>Data Group Perhiasan</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?= base_url('supplier') ?>" class="nav-link">
								<i class="fa fa-medal nav-icon"></i>
								<p>Data Barang</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?= base_url('akun') ?>" class="nav-link <?= $active == 'akun' ? 'active':'' ?>">
								<i class="fa fa-users nav-icon"></i>
								<p>Data User / Pemakai</p>
							</a>
						</li>
					</ul>
				</li>
				<li class="nav-item<?= $active == 'transaksi_pembelian' ? ' menu-open has-treeview':'' ?>">
					<a href="#" class="nav-link">
						<i class="nav-icon fa fa-shopping-cart"></i>
						<p>
							Transaksi
							<i class="right fas fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?= base_url('transaksi_pembelian') ?>" class="nav-link <?= $active == 'transaksi_pembelian' ? 'active':'' ?>">
								<i class="fa fa-cart-plus nav-icon"></i>
								<p>Pengadaan Barang</p>
							</a>
						</li>
					</ul>					
				</li>
				<li class="nav-item">
					<a href="#" class="nav-link">
						<i class="nav-icon fas fa-copy"></i>
						<p>
							Laporan
							<i class="right fas fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">

					</ul>
				</li>
				<li class="nav-item">
					<a href="<?= base_url('logout') ?>" class="nav-link">
						<i class="nav-icon fas fa-sign-out-alt"></i>
						<p>
							Logout
						</p>
					</a>
				</li>				
				<?php } else if($this->session->userdata('role') == 'Operator'){ ?>
				<li class="nav-item">
					<a href="<?= base_url('transaksi_penjualan') ?>" class="nav-link <?= $active == 'transaksi_penjualan' ? 'active':'' ?>">
						<i class="nav-icon fa fa-laptop"></i>
						<p>
							Transaksi Penjualan
						</p>
					</a>
					<a href="<?= base_url('logout') ?>" class="nav-link">
						<i class="nav-icon fas fa-sign-out-alt"></i>
						<p>
							Logout
						</p>
					</a>
				</li>
				<?php } ?>
			</ul>
		</nav>
		<!-- /.sidebar-menu -->
	</div>
	<!-- /.sidebar -->
</aside>
