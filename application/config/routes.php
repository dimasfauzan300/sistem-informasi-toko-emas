<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'AkunController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//Route Akun
$route['login']['POST'] = 'AkunController/login';
$route['logout']['GET'] = 'AkunController/logout';
$route['akun']['GET'] = 'AkunController/tampilDataAkun';
$route['akun/dataTable']['POST'] = 'AkunController/getDataTable';
$route['akun/edit/(:num)']['GET'] = 'AkunController/editAkun/$1';
$route['akun/simpan']['POST'] = 'AkunController/simpanAkun';
$route['akun/update']['PUT'] = 'AkunController/updateAkun';
$route['akun/delete/(:num)']['DELETE'] = 'AkunController/deleteAkun/$1';

//Route Supervisor
$route['supervisor/beranda']['GET'] = 'SupervisorController';

//Route Suppliers
$route['supplier']['GET'] = 'SupplierController/index';
$route['supplier/dataTable']['POST'] = 'SupplierController/getDataTable';
$route['supplier/edit/(:num)']['GET'] = 'SupplierController/editSupplier/$1';
$route['supplier/simpan']['POST'] = 'SupplierController/simpanSupllier';
$route['supplier/update']['PUT'] = 'SupplierController/updateSupllier';
$route['supplier/delete/(:num)']['DELETE'] = 'SupplierController/deleteSupllier/$1';

//Route Jenis Perhiasan
$route['jenis_perhiasan']['GET'] = 'JenisPerhiasanController/index';
$route['jenis_perhiasan/dataTable']['POST'] = 'JenisPerhiasanController/getDataTable';
$route['jenis_perhiasan/edit/(:num)']['GET'] = 'JenisPerhiasanController/editJenisPerhiasan/$1';
$route['jenis_perhiasan/simpan']['POST'] = 'JenisPerhiasanController/simpanJenisPerhiasan';
$route['jenis_perhiasan/update']['PUT'] = 'JenisPerhiasanController/updateJenisPerhiasan';
$route['jenis_perhiasan/delete/(:num)']['DELETE'] = 'JenisPerhiasanController/deleteJenisPerhiasan/$1';

//Route Group
$route['group']['GET'] = 'GroupController/index';
$route['group/dataTable']['POST'] = 'GroupController/getDataTable';
$route['group/edit/(:num)']['GET'] = 'GroupController/editGroup/$1';
$route['group/simpan']['POST'] = 'GroupController/simpanGroup';
$route['group/update']['PUT'] = 'GroupController/updateGroup';
$route['group/delete/(:num)']['DELETE'] = 'GroupController/deleteGroup/$1';


//Route Transaksi
$route['transaksi_pembelian']['GET'] = 'TransaksiController/indexPembelian';
$route['transaksi_penjualan']['GET'] = 'TransaksiController/indexPenjualan';
