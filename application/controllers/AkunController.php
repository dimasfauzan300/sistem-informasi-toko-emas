<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AkunController extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Akun');
		$this->load->library('Datatables');
	}

	public function index()
	{
		$this->load->view('akun/login');		
	}
	

	public function login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$dataLogin = $this->Akun->cekLogin($username, $password);		
		if($dataLogin)
		{
			$this->session->set_userdata($dataLogin);			
			if($dataLogin['role'] == 'Supervisor')
			{				
				redirect(base_url('supervisor/beranda'));
			}else if($dataLogin['role'] == 'Operator')
			{
				redirect(base_url('transaksi_penjualan'));
			}
		}else{
			$this->session->set_flashdata('notif', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Username/Passwords Salah</div>");
			redirect(base_url());
		}
	}

	public function logout()
	{		
		$this->session->sess_destroy();
		redirect(base_url());
	}

	//Manajemen Akun

	public function tampilDataAkun()
	{
		$data['title'] = 'Data Master Akun';
		$data['layout'] = 'akun/akun_view';
		$data['active'] = 'akun';
		$this->load->view('template', $data);
	}

	public function getDataTable()
	{
		if($this->session->userdata('role') != 'Supervisor')
		{
			$this->session->set_flashdata('notif', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Forbidden</div>");
			redirect(base_url());
		}

		header('Content-Type: application/json');
		echo $this->Akun->getDataTable();
	}

	public function editAkun($id)
	{
		if($this->session->userdata('role') != 'Supervisor')
		{
			$this->session->set_flashdata('notif', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Forbidden</div>");
			redirect(base_url());
		}

		$data = $this->Akun->getAkun($id);
		echo json_encode($data);
	}

	public function simpanAkun()
	{
		if($this->session->userdata('role') != 'Supervisor')
		{
			$this->session->set_flashdata('notif', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Forbidden</div>");
			redirect(base_url());
		}

		$data = [
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'role' => $this->input->post('role'),
		];
	
		$this->Akun->simpanAkun($data);

		echo json_encode(['OK']);
	}

	public function updateAkun()
	{
		if($this->session->userdata('role') != 'Supervisor')
		{
			$this->session->set_flashdata('notif', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Forbidden</div>");
			redirect(base_url());
		}
		
		$id = $this->input->input_stream('id_akun');
		
		$data = [
			'username' => $this->input->input_stream('username'),
			'password' => $this->input->input_stream('password'),
			'role' => $this->input->input_stream('role'),
		];
		$this->Akun->updateAkun($id, $data);
		echo json_encode(['OK']);
	}

	public function deleteSupllier($id)
	{
		if($this->session->userdata('role') != 'Supervisor')
		{
			$this->session->set_flashdata('notif', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Forbidden</div>");
			redirect(base_url());
		}

		$this->Akun->deleteAkun($id);
		echo json_encode(['OK']);
	}

}
