<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SupervisorController extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('role') != 'Supervisor')
		{
			$this->session->set_flashdata('notif', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Forbidden</div>");
			redirect(base_url());
		}
	}
	

	public function index()
	{
		$data['title'] = 'Beranda';
		$data['layout'] = 'supervisor/beranda';
		$data['active'] = 'beranda';
		$this->load->view('template', $data);		
	}

}
