<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GroupController extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('role') != 'Supervisor')
		{
			$this->session->set_flashdata('notif', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Forbidden</div>");
			redirect(base_url());
		}
		$this->load->model('Group');
		$this->load->library('Datatables');
	}
	

	public function index()
	{
		$data['title'] = 'Data Master Group';
		$data['layout'] = 'group/group_view';
		$data['active'] = 'group';
		$this->load->view('template', $data);
	}

	public function getDataTable()
	{
		header('Content-Type: application/json');
		echo $this->Group->getDataTable();
	}

	public function editGroup($id)
	{
		$data = $this->Group->getGroup($id);
		echo json_encode($data);
	}

	public function simpanGroup()
	{
		$data = [
			'nama_group' => $this->input->post('nama')
		];
	
		$this->Group->simpanGroup($data);

		echo json_encode(['OK']);
	}

	public function updateGroup()
	{		
		$id = $this->input->input_stream('id_group');
		
		$data = [
			'nama_group' => $this->input->input_stream('nama')
		];		

		$this->Group->updateGroup($id, $data);
		echo json_encode([$this->input->input_stream()]);
	}

	public function deleteGroup($id)
	{
		$this->Group->deleteGroup($id);
		echo json_encode(['OK']);
	}


}
