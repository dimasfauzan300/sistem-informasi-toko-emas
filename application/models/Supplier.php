<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Model {	

	public function getDataTable()
	{
		$this->datatables->select('id_supplier, nama, alamat, kota, telp');
		$this->datatables->from('suppliers');		
		$this->datatables->add_column('action', '<a href="#" class="btn btn-info" data-id="$1" data-toggle="modal" data-target="#modelId" id="buton_edit"><i class="fa fa-edit"></i> Edit</a> <a href="#" class="btn btn-danger" data-id="$1" id="buton_hapus"><i class="fa fa-trash"></i> Hapus</a>', 'id_supplier');
		return $this->datatables->generate();
	}

	public function getSupplier($id = "")
	{
		if($id)
		{
			$this->db->select('nama, alamat, kota, telp');
			return $this->db->get_where('suppliers', ['id_supplier' => $id])->row_array();			
		}else{
			return $this->db->get('suppliers')->result_array();			
		}
	}

	public function simpanSupplier($data)
	{
		$this->db->insert('suppliers', $data);		
	}

	public function updateSupplier($id, $data)
	{
		$this->db->update('suppliers', $data, ['id_supplier' => $id]);		
	}

	public function deleteSupplier($id)
	{
		$this->db->delete('suppliers', ['id_supplier' => $id]);		
	}

}

/* End of file Supplier.php */
