<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Akun extends CI_Model {
		
	public function cekLogin($username, $password)
	{
		$this->db->select('username, role');
		$where = ['username' => $username, 'password' => $password];
		return $this->db->get_where('akun', $where)->row_array();
		
	}

	public function getDataTable()
	{
		$this->datatables->select('id_akun, username, password, role');
		$this->datatables->from('akun');		
		$this->datatables->add_column('action', '<a href="#" class="btn btn-info" data-id="$1" data-toggle="modal" data-target="#modelId" id="buton_edit"><i class="fa fa-edit"></i> Edit</a> <a href="#" class="btn btn-danger" data-id="$1" id="buton_hapus"><i class="fa fa-trash"></i> Hapus</a>', 'id_akun');
		return $this->datatables->generate();
	}

	public function getAkun($id = null)
	{
		if($id)
		{
			$this->db->select('username, password, role');
			return $this->db->get_where('akun', ['id_akun' => $id])->row_array();			
		}else{
			return $this->db->get('akun')->result_array();			
		}
	}

	public function simpanAkun($data)
	{
		$this->db->insert('akun', $data);
	}

	public function updateAkun($id, $data)
	{
		$this->db->update('akun', $data, ['id_akun' => $id]);		
	}

	public function deleteAkun($id)
	{
		$this->db->delete('akun', ['id_akun' => $id]);		
	}
}
